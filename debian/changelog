python-django-bootstrap-form (3.4-9) unstable; urgency=medium

  * d/rules: Export DJANGO_SETTINGS_MODULE env when testing (Closes: #1080129).

 -- Sunil Mohan Adapa <sunil@medhas.org>  Fri, 30 Aug 2024 21:01:39 -0700

python-django-bootstrap-form (3.4-8) unstable; urgency=medium

  [ Pushkar Kulkarni ]
  * Move away from distutils, use packaging (Closes: #1062980)

  [ Sunil Mohan Adapa ]
  * d/control: Update standards version to 4.6.2 (no changes needed)
  * d/rules: Remove dh_auto_test override
  * d/*: Drop dependency on openstack packaging tools
  * d/tests/control: Use pytest for running tests

  [ James Valleroy ]
  * Fix Forwarded field for patch
  * Update Copyright year and name

 -- James Valleroy <jvalleroy@mailbox.org>  Fri, 09 Feb 2024 15:29:10 -0500

python-django-bootstrap-form (3.4-7) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + python-bootstrapform-doc: Add Multi-Arch: foreign.

  [ James Valleroy ]
  * Set Standards-Version to 4.6.1
  * Add my copyright years
  * Use runtests.py instead of setup.py to run tests

 -- James Valleroy <jvalleroy@mailbox.org>  Sat, 03 Dec 2022 12:39:58 -0500

python-django-bootstrap-form (3.4-6) unstable; urgency=medium

  [ Sunil Mohan Adapa ]
  * Add support for Django 4.0. Drop support for old Django/Python versions.
  * Remove older, unnecessary patch.

 -- James Valleroy <jvalleroy@mailbox.org>  Tue, 28 Jun 2022 17:20:27 -0400

python-django-bootstrap-form (3.4-5) unstable; urgency=medium

  * d/control: Mark Multi-Arch for doc package
  * d/gbp.conf: Disable pristine-tar

 -- James Valleroy <jvalleroy@mailbox.org>  Thu, 16 Sep 2021 12:20:54 -0400

python-django-bootstrap-form (3.4-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + Build-Depends: Drop versioned constraint on openstack-pkg-tools.

  [ Salman Mohammadi ]
  * change package name python3-bootstrapform to python3-django-bootstrapform
  * change package python-bootstrapform-doc to python-django-bootstrapform-doc
  * make python3-bootstrapform a transitional package
  * make python-bootstrapform-doc a transitional package

  [ James Valleroy ]
  * Bump debhelper from old 12 to 13.
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete field Name from debian/upstream/metadata (already present in
    machine-readable debian/copyright).
  * Update standards version to 4.5.0, no changes needed.
  * d/watch: Set version to 4
  * d/control: Set standards version to 4.6.0
  * d/control: Set Rules-Requires-Root: no

 -- James Valleroy <jvalleroy@mailbox.org>  Thu, 02 Sep 2021 08:53:43 -0400

python-django-bootstrap-form (3.4-3) unstable; urgency=medium

  [ James Valleroy ]
  * Add salsa CI
  * debian: Drop python2 package (Closes: #937705).
  * debian: Switch to pybuild
  * debian: Switch to debhelper-compat=12
  * d/control: Update standards version to 4.4.0
  * debian: Add upstream/metadata
  * d/rules: Remove unneeded overrides
  * d/patches: Add patch to fix tests during build
  * d/rules: Remove dh auto test override
  * d/control: Add myself to uploaders
  * debian: Use upstream tests for autopkgtest

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Oct 2019 22:40:49 +0200

python-django-bootstrap-form (3.4-2) unstable; urgency=medium

  [ Sunil Mohan Adapa ]
  * Add explicit dependency on python3-distuils.

 -- Federico Ceratto <federico@debian.org>  Sun, 25 Mar 2018 18:47:14 +0100

python-django-bootstrap-form (3.4-1) unstable; urgency=medium

  [ Sunil Mohan Adapa ]
  * New upstream release. Add support for Django 2.0 and Python 3.6.

 -- Federico Ceratto <federico@debian.org>  Thu, 15 Mar 2018 19:51:10 +0000

python-django-bootstrap-form (3.3-1) unstable; urgency=medium

  [ Sunil Mohan Adapa ]
  * New upstream release (Closes: #876974).
  * d/control: Change maintainer to FreedomBox Packaging Team. Many
    thanks to Open Stack team for maintaining so far.
  * Refresh patches:
    - django-1.10-fix-settings-in-runtests.py.patch
  * Remove patches:
    - django-1.10-add-Django-1.10-fixtures.html.patch
  * Standards-Version is now 4.1.2.
  * Build depend on python3-sphinx instead of python-sphinx.
  * Update VCS URLs to point to new FreedomBox repositories.

 -- Federico Ceratto <federico@debian.org>  Sat, 16 Dec 2017 13:09:37 +0000

python-django-bootstrap-form (3.2.1-2) unstable; urgency=medium

  * Fixed Forwarded fields in Django 1.10 patches.
  * Fixed the fixture html files (Closes: #834469).

 -- Thomas Goirand <zigo@debian.org>  Tue, 26 Jul 2016 09:22:36 +0000

python-django-bootstrap-form (3.2.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed homepage (https).
  * Fixed VCS URLs (https).
  * d/rules: Changed UPSTREAM_GIT protocol to https
  * d/copyright: Changed source URL to https protocol

  [ Thomas Goirand ]
  * New upstream release.
  * Removed all patches.
  * Added patches (Closes: #828661):
    - django-1.10-fix-settings-in-runtests.py.patch
    - django-1.10-add-Django-1.10-fixtures.html.patch
  * Standards-Version is now 3.9.8 (no change).
  * Fixed debian/copyright ordering.

 -- Thomas Goirand <zigo@debian.org>  Mon, 18 Jul 2016 07:59:44 +0000

python-django-bootstrap-form (3.1.0-6) unstable; urgency=medium

  * Switch from syncdb to migrate, which fixes FTBFS (Closes: #806362).
  * Watch file now using the PyPi redirector.
  * Standards-Version bumped to 3.9.6 (no change).

 -- Thomas Goirand <zigo@debian.org>  Wed, 23 Dec 2015 09:48:16 +0000

python-django-bootstrap-form (3.1.0-5) unstable; urgency=medium

  * Added Python 3 support (Closes: #761294).
  * Added doc-base registration.

 -- Thomas Goirand <zigo@debian.org>  Sun, 14 Sep 2014 18:20:33 +0800

python-django-bootstrap-form (3.1.0-4) unstable; urgency=medium

  * Added patches for Django 1.7 (Closes: #755656). Thanks to Raphael Hertzog
    for the bug report and patches.

 -- Thomas Goirand <zigo@debian.org>  Sun, 07 Sep 2014 07:37:50 +0000

python-django-bootstrap-form (3.1.0-3) unstable; urgency=medium

  * Do not package the test folder (Closes: #733729).

 -- Thomas Goirand <zigo@debian.org>  Fri, 03 Jan 2014 17:46:38 +0800

python-django-bootstrap-form (3.1.0-2) unstable; urgency=medium

  * Added missing python-django build-depends.

 -- Thomas Goirand <zigo@debian.org>  Mon, 30 Dec 2013 01:51:49 +0000

python-django-bootstrap-form (3.1.0-1) unstable; urgency=low

  * Initial release. (Closes: #730463)

 -- Thomas Goirand <zigo@debian.org>  Mon, 25 Nov 2013 17:44:33 +0800
